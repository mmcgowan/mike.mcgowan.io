+++
author = "Mike McGowan"
title = "Certified ScrumMaster Training"
date = "2015-06-29"
description = "Getting a certification in something you feel you are alreday a practionier"
tags = [
    "Scrum",
    "Agile",
]
+++

Despite the fact that I have been a Agile Practitioner for many years and while I sent (read paid for) many employees to attend classes and certification tests over the years, I had never done so myself. A few weeks back I rectified that and attended CSM training/certification from the Scrum Alliance. I went into the class thinking that much of this would be repetitious (parts of it were), that there would be beginners (there were) and that the exercises would be useless (they *weren't*). Each of those points is worth a quick paragraph.

# Material

It's never a bad idea to review the assumptions and principles on any material. In this case I found the most valuable was review of the roles of a Scrum team. I was able to apply this to my current projects and realized that in at least one case, I really do not have a Product Owner. One attendee asked, "Without a Product Owner, do you really have a project?" This is an appropriately challenging question. It was somewhat encouraging to hear that I was not alone in this challenge.

# Other Attendees
There were several attendees that had recently been laid off and as part of their severance packages included some training dollars. Several of them were classic PMP's from the PMI. They were struggling with the basics of SCRUM since the material was very different from their backgrounds. However, their questions were valuable as they allowed the instructor to underscore key points and assumptions. The experienced Agilists were also add some stories from the front lines that added to the discussion. As with all training that allows you to "get away", the real value is in hearing those real world stories -- what have other people tried and not tried, what has work or not worked, etc.

# Exercises
I take my hats off to the Scrum Alliance on the exercises they include in their training. These no doubt have been refined over the years. But given the relatively short time frame of a two day class (you take the certification test on your own after the class) the exercises most definitely gave us a chance to quickly plan, execute, adapt and repeat. It was both fascinating to watch and be a part of. In nearly all of the cases it taught me somethings(s) about myself.


Over the next couple of blog entries, I will share more about what I learned about myself and how I will apply it to my work.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
