+++
author = "Mike McGowan"
title = "Rich Content with Markdown and Hugo"
date = "2020-10-28"
description = "A brief description of Hugo Shortcodes"
tags = [
    "shortcodes",
    "privacy",
]
+++

Over the last couple of months, I have been dabbling with [Hugo](https://gohugo.io/) an open source tool for generating static websites. I got interested in it coming from Wiki syntax, then to markdown syntax and it was suggested as a quick way to render the markdown content. I had gotten spoiled with the my favorite Wiki's abilty to included Rich Content, so I spent some time learning what and how Hugo can do similarly.

<!--more-->

Hugo ships with several [Built-in Shortcodes](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes) for rich content, along with a [Privacy Config](https://gohugo.io/about/hugo-and-gdpr/) and a set of Simple Shortcodes that enable static and no-JS versions of various social media embeds.

---

## YouTube Privacy Enhanced Shortcode

{{< youtube ZJthWmvUzzc >}}

<br>

---

## Twitter Simple Shortcode

{{< twitter_simple 1085870671291310081 >}}

<br>

---

## Vimeo Simple Shortcode

{{< vimeo_simple 48912912 >}}
