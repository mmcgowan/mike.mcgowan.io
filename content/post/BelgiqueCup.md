+++
author = "Mike McGowan"
title = "The Belgique Cup"
date = "2024-05-23"
description = "The Legend behind the Belgique Cup"
tags = [
    "legends",
]
+++

## This years contenders for the Belgique Cup

With the Stanley Cup finals approaching, it's time to talk about the teams in contention for the Belgique Cup. The Belequie Cup (aka "The Cheeks Cup") is awarded each year to the team that "wins" the "loser's ladder". A team advances towards the Cup when the team that ousted you from the Stanley Cup playoffs loses in the next round. If you are not familiar with The Belgique Cup's namesake, see the section on the Legend of Patrice Belgique.

The four contenders are:

- NY Islanders, if the FL Panthers lose the Cup final to the Western Champ
- Toronto Leafs if the NY Rangers lose the Cup final to the Western Champ
- LA Kings if the Dallas Stars lose the Cup final to the Eastern Champ
- Winnipeg Jets if the Edmonton Oilers lose the Cup final to the Eastern Champ 

## The Legend of the Patrice "The Check" Belgique

> Back in 1941 Patrice "The Cheek" Belgique was about to embark upon his NHL career. How far he would have gotten is impossible to ascertain

> After graduating from the Saskatchewan junior leagues, Belgique moved to Boston where he played with the Boston Olympics of the EHL. A clean right winger with a knack for scoring goals, Belgique impressed in the EHL, scoring 29 goals in 43 games. His goal scoring exploits saw him get a promotion to the NHL Boston Bruins. He played in 10 games. He scored 1 goal and 2 assists in limited ice time.

> With more and more players being summoned to fight in World War II, it was hoped that Belgique could step in and score some big goals during the War years. However in 1942, Belgique too was called off to serve with the Canadian military.

> Belgique served 4 years in the military, playing hockey when he could. When he returned from the war for the 1946-47 season, "The Cheek" tried out with the AHL's Hershey Bears. He struggled to find his pre-war form in the two years with the Bears, and soon found a home in a lower minor league, the USHL, with the Tulsa Oilers. Belgique played strongly in the weaker league, scoring 81 goals in 3 years before hanging up the blades.

# The Cheeks Fan Club

If you were not familiar with Patrice Belgique, it's even more unlikely that you know there is a fan club named what else - "The Cheeks". Pictured below are a couple of photos of the current president of The Cheeks.

![Honoring the Greats](/images/HonoringTheGreats.jpeg)
Visiting the Hockey Hall of Fame.

![The Cheeks Nbr 1 Fan](/images/TheCheeksNbr1Fancopy.jpeg)


