+++
author = "Mike McGowan"
title = "Courage to Say No"
date = "2023-09-01"
description = "Getting to 'no' takes courage"
tags = [
    "courage", "focus"
]
+++

I have heard a lot of talk at work that "We are trying to do way too much work." Some thoughts inspired by a recent [blog from Seth Godin, "Getting to no"](https://seths.blog/2023/09/getting-to-no/), and the thought that "yes is magical". 

The company still has a startup mindset and that anything is possible because we are "innovators". We have tried various methods to focus our "yeses" with marginal success.
<!--more--> So, perhaps more important then, is to learn how to say "no". Seth Godin's thoughts on qualities of a generous "no":

- We say it to someone we care about.
- We offer insight.
- Tell them the truth.
- Celebrate what we can do.

Why would anyone do otherwise?