+++
author = "Mike McGowan"
title = "The Things I Carry"
date = "2023-11-27"
description = "What do we carry around that weighs us down"
tags = [
    "focus",
]
+++

Over the weekend I finished reading [The Things They Carried](https://www.amazon.com/Things-They-Carried-Tim-OBrien/dp/0618706410), by Tim O'Brien. (I highly recommend.) As you can imagine it's about the things that weigh us both physically and metaphorically.

Since it was a long weekend, I had some time to think about what's "weighing me down". Here's a short list: <!--more-->

## Physical Stuff

- I've been in my house for several decades. Despite a resolution when I knocked down a major decade to remove "an old thing" from the house, when "a new thing" showed up, there is still too much stuff in the house.
- Since it's just my  wife and I in the house, the focus now is on getting our kids things out of the house. Today, I hope to drop off my daughter's Christmas ornaments.
- My den is a monument to when paper ruled the day. I've got a new shredder, and I am slowly shredding things I long ago did not need - credit card statements from 12 years ago!
- Stress - despite long ago, internalizing [David Allen's mantra, "Stress comes from not knowing where your stuff is"](https://www.amazon.com/Getting-Things-Done-Stress-Free-Productivity/dp/0142000280) (me paraphrasing), I still think I can get more down in 24 hours than is possible - very stress inducing.


## Metaphorical Stuff

- Have worked longer than I have been in my house, there a lot of work stories, lessons learned. Following the lead of [Tim O'Brien](https://en.wikipedia.org/wiki/Tim_O%27Brien_(author)), I may write some of those down to "lighten my load".
- How I try and get stuff done - despite being (thinking?) I am a change junkie; I can be a creature of habit. I frequently need to remind myself there are plenty of new things to learn.
- The previous bullet reminds me that a desire to learn can lead to never ending chasing "shiny new objects". I'll want to be more thoughtful about which shiny things to chase.

That's my "time boxed" short list. What are you carrying that weighs you down?