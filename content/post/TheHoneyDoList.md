 +++
author = "Mike McGowan"
title = "The Honey-Do List"
date = "2024-02-23"
description = "The list never ends" 
tags = [
    "retirement",
    "volunteering"
]
+++

I recently got together with several colleagues from my last two employers. I have been retired for about six weeks now and everyone was asking what I had been doing. Yesterday, I started on two things: the honey-do list and a first volunteer effort. 

## Honey-Do List

One of my former colleagues said when he retires, he wants to so without letting his wife know. Among the men in attendance, the feeling was that this list will never end. Mine has existed for some time now as we have several different maintenance tasks that need to be done on the house. I have often said that my best skill is knowing what I should not be doing. I put things on the list I have the ability to do. However, there will be a few other things I should know how to do or can do, and I’ve just never had the time. Truth be told, I don’t mind doing things when I’m not rushed or have the time.

Yesterday's task was painting the laundry room. It's been awhile since I took a full day painting around the house. It "inspired" the following tweet.

> Painted a room at home today. How did we ever do a task like painting all day before music streaming services existed? #JustAsking

The beauty of the tasks on the list is that I don’t have a hard deadlines and take my time.I will try and enjoy them as much as I possibly can. If I find out I’m not enjoying myself, I’ll try to remember that I’m lucky to be physically able to do them.

## Volunteering

That's my next post. See [So You Want to Volunteer.](../Volunteeering)