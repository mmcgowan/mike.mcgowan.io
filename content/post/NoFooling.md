+++
author = "Mike McGowan"
title = "No Foolin' - Privacy vs. Convenience"
date = "2024-04-01"
description = "Many Apps and Services provide great convenience, but make no mistake you are trading with your privacy "
tags = [
    "privacy",
    "security",
]
+++

## An interesting day to update a privacy policy

This morning, I received an updated privacy policy for a service (just "apps" for the rest of this post) that I use. Since it was April Fools’ Day, I thought it was as good a time as any to review their privacy policy. I focused on the use of data, and I’ve clipped a relevant quote below (app name has been removed). 

> Under certain data protection laws like GDPR, companies must have a “legal basis”—a valid reason—to process personal information. *App Name* relies on different legal bases to process your information for the purposes described in this Privacy Policy. We use the Information We Collect—Account information, Content you create, Payment information, Information from actions you take, Information used to enable optional features, Other information you provide directly to us, Information about your device, Information about your use of the apps or websites, other information that we collect automatically, and Information we receive from other sources—for the following reasons and according to these legal bases. For each reason, we describe why we process your information and how we process your information to achieve each purpose.

That said before signing up, it’s not a bad idea to do a quick review of any services or apps privacy policy. I am also going to try and least read the email summaries of future updates for all the apps I use. 

It’s never a bad time to review policies for apps we use, but who has the time? If I read all the policy statements on this app, it would require about 30 minutes. If you multiply that by the number of apps you use, who would have time to do anything else?

## As luck would have it

It was somewhat serendipitous that I was looking at a new feature for another app. It made me curious why anyone would use this new feature. While reviewing, I stumbled onto a post that this apps use of location data was invasive. I generally limit location data to  “share while using app” but this particular app had a separate setting for its location data. I promptly turned it off.

Many of the apps and services we use on a daily basis are very useful, but never forget you are almost always trading privacy for convenience.