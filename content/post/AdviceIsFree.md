+++
author = "Mike McGowan"
title = "Advice is Free"
date = "2024-02-12"
description = "My kids ask family, friends, and neighbors for advice for me"
tags = [
    "retirement",
    "focus",
]
+++

Last weekend, my family threw a retirement party for me - very nice of them! As a "party thing", they put out a sheet with some questions. The heading - ADVICE & WISHES for Mike's retirement.  Here is a collection of the answers. <!--more-->

- **BE SURE TO** - Stop thinking about work (they have stopped thinking about you).

- **NEVER** - Worry if you have enough.

- **REMEMBER**- Your wife, kids, and perhaps future family. They all love you. 

- **ENJOY** - New hobbies & a few new beers.

- **VISIT** - Every park, a fun new place with your family.

- **ALWAYS** - Keep being you.

Advice is free and it looks like great advice. Thanks one and all.
