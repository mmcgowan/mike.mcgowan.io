+++
author = "Mike McGowan"
title = "Agility, Demos, and Credibility with your Customer"
date = "2014-07-15"
description = "A story about what can go wrong (and right) with a demo."
tags = [
    "Agile"
]
+++

Once I had the opportunity to present at my company's annual User Conference. The Product Manager for the session set a goal that in hindsight was a pretty aggressive demonstration. When he walked through the outline, I thought, "Yep, that can be done" and the synopsis went out to the conference attendees. As frequently happens, work and life got in the way of the preparation time. 
<!--more--> I was fortunate enough to get some fantastic assistance from my colleagues on the requisite slides and that helped to further focus "the story" that the demonstration. I entered the conference feeling reasonably prepared.

For some reason the day before the presentation, "the buzz" around what I was going to demonstrate began to build. There was a Field Services employee that read the session synopsis and said, "You are going to do *that* during a live demonstration-are you crazy?" Shortly before the presentation, I ran into a customer that had intended to come, but then had another commitment. He apologized by saying that he was going to have to miss my "high wire act".  I was suddenly nervous.

We started the session with the usual slides to introduce the demo which was an effectively a 15-20 minute "product upgrade". So if there was any "high wire" in the session it was that we kicked of the upgrade script and went back to more slides while it was running. I had no real way to check on the progress of the upgrade without showing it to the entire audience. I tried to build the suspense with "Let's go if it worked" (I was fully expecting that it would). It did not.

I used the next few minutes to:

* Assess where we failed in the process -- some live triage if you will.

* Talk about where the failure occurred -- and why that aspect of the upgrade was critical for the customer.

* Further assess if enough of the upgrade worked, so that downstream demonstration could continue -- thankfully it did. 

The session ended with the usual Q&A, some of which was possible because of the demonstration failure. The session ended with my pulse and blood pressure at reasonably normal levels.

Later on the show floor one of the attendees waited patiently for me to finish speaking with another customer. It did not give me a good feeling. When we met, this individual said, “I just wanted to thank you for doing that demonstration”. I replied, “Well it did not work like I had planned”. The reply, “Well, that’s why we knew you we really running a full upgrade. If it had worked, we would not have believed you were really doing what you said you were attempting. We knew you were doing what we have to do. What was important was that you had the confidence to try.” Never had a demonstration failure given me such credibility — at least in the eyes of one attendee.
