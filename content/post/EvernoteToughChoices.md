+++
author = "Mike McGowan"
title = "Evernote - Reinventing Themselves with Tough Choices and Standards"
date = "2020-10-12"
description = "Evernote makes some tough choices"
tags = [
    "evernote",
    "standards",
]
+++

I am sharing a link to a video interview that was done with Ian Small, the Evernote CEO, Ian Small.    He has been doing a [series of videos and sharing them](https://www.youtube.com/playlist?list=PL4I5cq2DfrSpehLO_71NCjKSZE0nGXIvf) with the Evernote user base (of which I am).    Aside from me being a user, I am interested in this for two reasons:

- The series of videos Ian started in early 2019 shortly were to announce that there would be no new features in Evernote for all of 2019.    A risky move for a new CEO, so the videos were a ["behind the scenes"](https://www.youtube.com/playlist?list=PL4I5cq2DfrSpehLO_71NCjKSZE0nGXIvf) view of what they were working on to make Evernote better

- As mentioned in the video, Evernote had worked itself into a position of not being able release any features let alone innovate. This point is what the rest of this post will be about.

<!--more-->

What contributed to this point of technically stalled? I noted three things from the video (~35 minutes if you have the time):

1. Every platform (clients: Web, Windows, Mac, IOS, Androids) had its own Product Manager. Over time, each of them really felt like a different product from a user point of view. This feature drift in the clients made it extremely difficult to implement any back-end changes.

1. Any noteworthy feature change had to be implemented 5 times. For company of their size, which is most definitely not scalable.   He says they are now writing 1/3 the amount of code that they used to.

1. To address the previous bullet, they made a development language / framework choice - Electron JS. Ian Small describes this as a "religious war" that would never be settled. However, as a leader a choice needed to me made for the company to move forward. He feels they now in position to release major new features again. In short, they can innovate again while maintaining the stability that 250 million users expect (demand really since there are some many other competitors.

{{< youtube dEioUvE52To >}}

How is it going so far? Too early to tell. I updated one of the five clients (I use all five) and while can get over the many of the functional changes (hot keys going away/changing, UI reorganized, toolbar gone, etc.) I am finding the editor and the UX responsiveness a bit pokey. Until I feel better about this, I am going to hold off updating the other clients. I am looking forward to seeing features again and to underscore my larger point, Evernote could not have gotten to that point without setting a strategy, following it, setting standards and implementing them.



