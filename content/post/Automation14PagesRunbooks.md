+++
author = "Mike McGowan"
title = "Automation, 14 Page Documents, and Runbooks"
date = "2019-10-09"
description = "A story about automation and playbooks"
tags = [
    "Agile",
    "automation",
]
+++

Several years ago I was involved in the automating a rather large ERP system. Traditionally, that ERP system was installed by technical consultants who spent five days installing it while billing at $150 an hour. The automation effort took reduced that effort to 4 hours of automation run-time with 4-8 hours of manual "post install steps".  The manual steps could be broken down to 4 hours for products that were not automated and another 1-4 based on variable optional add on products.   For these manual "post-install" products, the development team produced a 14 page MS word document on how to complete the additional steps.    The VP in charge had questions.   It went something like this:

*VP: "Why is there a document at all and why does this took a full day?*

*Yours truly: "Because it used to take 5 days and this a first automation iteration."*

*VP: "OK, why 14 pages?"*

*Yours truly: "Because the consultant that used to do this had 5 manuals at 100 pages each."* 

*VP: "OK, but let's make that document smaller."*

I had the opportunity to take the 14 page document into the operations room and walk through some testing of the automated provisioning process.    Two individuals from Operations were doing "Pair Admin-ing"  one person was performing the tasks in the document and the other was taking notes.     When I asked what the person taking notes what he was doing,  he said "Producing a Runbook. This 14 page document is not nearly enough for us to have this work done in <insert off-shore location>."  How long was the runbook - 75 pages!

The development team was somewhat irritated, and did the usual, why are we producing a document when you end up re-doing it?!?  It had not dawned on them the assumed product knowledge they had (that existed only in their head) was the delta between 14 & 75. The operations team knew they were producing a document for someone that had zero product knowledge.   Eventually these two groups found a way to collaborate on this and eventually a single document got produced with each automation iteration that both groups were satisfied with.  And what of the VP?   The length of the document was never mentioned again, despite his assistance on calling it "the 14 page document".