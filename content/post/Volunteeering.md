+++
author = "Mike McGowan"
title = "So you want to volunteer - don't wait"
date = "2024-03-07"
description = "It's never too early to start, the need is great" 
draft = true
tags = [
    "retirement",
    "volunteering"
]
+++

At a recent happy hour gathering with some former colleagues, I was asked what I was planning to do with my time. One of things is volunteering. One attendee responded with, “yeah, I gotta get started on that.” If I had one regret, it was that an on premise job with a long commute did not allow me the time to do more of that. (Or so I told myself) Yes, I did some with the kids when they needed volunteer hours but I was inconsistent after that. Now retired, I plan me to do this consistently a couple of times per week. I would encourage any of you that are still working, don’t wait until you retire to find some consistent cause to volunteer with.

# Not everyone wants your help 

There are some organizations I have offered my help and not heard back. Like I said in my working days, “you can’t help someone that doesn’t want to be helped or doesn’t know they need help.” Don’t bang your head against the wall. There are plenty of good organizations out there that want and need your help. Choose to spend your time wisely.