+++
author = "Mike McGowan"
title = "Hope Springs Eternal"
date = "2024-03-22"
description = "It was the annual trip to spring training that always carries the hope that your team will win the World Series"
tags = [
    "baseball",
    "Travel",
]
+++

I recently returned from my annual baseball spring training trip. My team is the Minnesota Twins, and they train in Fort Myers, Florida. This is the fifth year I’ve made such a trip with friends from high school and my hometown. (see picture below of our group at the [Blue Jays facility - TD Park in Dunedin](https://www.milb.com/dunedin/ballpark/TDBallpark))

## What’s great about it

- Everyone is in a great mood.

- The weather is almost always beautiful.

- It’s the start of baseball and has a feeling of renewal.

- While you would like to see your team do well. It’s not super critical.

## It's not heaven, it's just Florida

There are some current realities that do keep your feet firmly on the ground:

- The tickets are getting expensive.

- The food and beverage prices are the usual outrageous prices. But then again, the vendors are in a great mood and it and makes the whole exchange worthwhile.

- This is the first year I’ve noticed an increasing awareness of the dangers of the sun. There’s definitely respect for being in the sun.

## The stadium experience

 I’ve had the opportunity to see six of the 15 different minor league parks in the [Grapefruit League](https://www.mlb.com/spring-training/grapefruit-league/map). I don’t think I’m saying this just because I’m a homer, but the [Twins complex in Fort Myers](https://www.mlb.com/twins/spring-training/ballpark) still ranks among the best of those I have seen. The twins did a nice job when they bought lots of land in Fort Myers in the early 90s. At least two of the parks I have seen (Toronto and Pittsburgh) are in neighborhoods and while it feels "quaint", those teams feel "land-locked" that seems limiting for today's Major League teams. That thought has me thinking it might be worth a post just on that idea.

 ![Twins at BlueJays](/images/TDPark_03172024.jpeg)