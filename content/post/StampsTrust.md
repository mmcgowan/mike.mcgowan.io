 +++
author = "Mike McGowan"
title = "Offsite -- Collaboration, Trust, and 'Stamps' around Agile Topics "
date = "2012-04-03"
description = "Collecting Stamps does not help build trust" 
tags = [
    "Trust",
    "collaboration",
]
+++

 I have spent nearly an entire day at an offsite meeting with seven of my development colleagues.   The day started with some discussion around communication, collaboration, and "bonding".     I was not anticipating a very positive day with the direction that the discussion was going.    Midway through the day, we made was I consider some very positive progress around two critical topics:

* Defined Acceptance Criteria for Software Consumers
* A milestone-based approach to Software Delivery based on defined acceptance criteria.

I considered these major breakthrough(s).    However, later in the day there was a specific topic that came up, that spawned some very passionate (read angry discussion) around the topics of collaboration, trust, and "stamps".    The latter term was suggested as a less inflammatory word than "trust".   The word "Stamps" being a reference to the long-ago retail practice of giving "stamps" that could be cashed in later.    The analogy is that we all collect "stamps" against each other to be "cashed in" at some later date.    This behavior was acknowledged, but as a group we came up with no good process to all cash in our stamps.

Unfortunately, I am leaving the offsite with the feeling that our "collective stamps" will kill any progress we made have made on Agile Topics -- collectively we don't seem to have "An Agile Mindset".
