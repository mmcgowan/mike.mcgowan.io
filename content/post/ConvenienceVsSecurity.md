+++
author = "Mike McGowan"
title = "A Trip to the Ballpark - Convenience vs. Security"
date = "2023-07-27"
description = "How long should it take to get into a baseball game?"
tags = [
    "security"
]
+++

Last year I got an email from my local MLB baseball team. They wanted me to sign up for a relatively well know service, that would allow me to get into the ballpark faster with my cell phone. All I needed to do was give them lot's of data about myself. I decided to pass and as a result, it took *long* time to get in the gate.
<!--more--> This year that same baseball team invested in some new AI technology. While I had not choice in the matter, it did significantly reduce the time it took to get inside the gate.
