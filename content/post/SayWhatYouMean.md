+++
author = "Mike McGowan"
title = "Agreeing on what we mean"
date = "2021-08-04"
description = "Agreeing on what we mean is a great place to begin." 
tags = [
    "communication",
    "collaboration",
]
+++

At a former job, it was suggested that I "always correct someone when they are using the wrong word".  While it was good that the speaker felt comfortable giving me that feedback "with the bark on", I think he confused my intent.  I was not suggesting the speaker was using the *wrong* word, I was suggest how I reacted, what I thought, and how I felt to the words being used.  When I verbalized that I suggested a new / different word.  

I thought of this story when I read ["Easily  Confused" by Seth Godin](https://seths.blog/2021/07/easily-confused/).  Here's the take home - _"Agreeing on what we mean is a great place to begin._"  For the record the words that started the discussion were: "cult" and "clan".  I suggested that word "community" might capture what the speakers were trying to say.  The reaction ("you will always correct someone") suggests they took my meaning as "you are wrong", "I am "correcting you".   Not my intent, but  _"Agreeing on what we mean is a great place to begin._"