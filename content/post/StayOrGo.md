+++
author = "Mike McGowan"
title = "Should I Stay or Should I Go Now"
date = "2024-06-04"
description = "After a year of frustration, an Evernote feature mysteriously starts working again."
tags = [
    "productivity",
    "evernote",
    "focus",
]
+++

The [decades old song from The Clash](https://youtu.be/xMaE6toi4mk?feature=shared) was running through my mind after a recent update from [Evernote](https://evernote.com/). I have been a long time user of EverNote. There are lots of complaints about recent price increases, but my biggest issue was that recurring tasks not working for me. When released, it was a feature that I was not particularly excited about. However, over time it became the most important thing that I used daily – my life became dependent on it. 

Unfortunately, I started to experience the infamous "spinning tasks" frequently discussed on the [user forums](https://discussion.evernote.com/). I reported this to support both before and after the Bending Spoons acquisition. The issue was never resolved. I even went so far as to delete all my tasks, both recurring and otherwise - still no luck. Taken together (the price increase and an important feature not working for me), I canceled my plan.

Yes, like many of you. It was frustrating listening to the complaints about the free versus trial plan. I’ve been loyal paying subscriber for the last decade or so. Imagine my surprise when the first time I fired up EverNote after cancelling my subscription tasks were working for me again. I share that for those of you who may be experiencing similar problems.

I am still on the fence as to whether or not I am going to take EverNote up on there special offers to renew this past spring. My fear is that once I pay, tasks will stop working again and I’ll have no recourse to recoup my investment. Perhaps I’ll start with a monthly payment to see what happens.