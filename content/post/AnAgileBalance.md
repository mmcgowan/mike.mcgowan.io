+++
author = "Mike McGowan"
title = "An Agile Balance"
date = "2015-09-28"
description = "Balancing Learning while continuously learning, adapting, and changing"
tags = [
    "Agile",
    "Scrum",
]
+++

I wrote a couple of posts back about my [ScrumMaster training](../certifiedscrummaster) that I would wrote more about how I would apply this to my job. This is the second such post.

This one is about the need to balance the discipline of what you are doing correctly with the ability to adapt, learn and change. If one is not careful, you might "discipline" as a static thing. In reality, while you are developing and practicing your discipline(s), you are at same time (or you should also be) learning and adapting. When viewed this way, discipline is a much more active endeavor. [In his slideshare titled "The Tao of Agile"](http://www.slideshare.net/tangram77/the-tao-of-agile-xp2012), [Fabio Armani](https://www.slideshare.net/tangram77?utm_campaign=profiletracking&utm_medium=sssite&utm_source=ssslideview) states that "Both (Lean and Agile) offer a thinking tool set that allow us create new models and different approaches".

Reflecting on one of my recent projects, I had the thought, "Finally, the Product Owner is starting to 'get it'. It's a good thing I was so disciplined about this!" When I went back to review with them what they had "finally learned", I could also see that my own approach had adapted and evolved. I was also "getting it" and learning things about the project that were not initially clear to me. This is what the an Agile MindSet is all about -- being open to this "thinking tool set".  Can you  recognize when new models & new approaches either need to happen or in fact have already happened -- either for you customer or yourself?

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
