 +++
author = "Mike McGowan"
title = "Hold Core Beliefs -- Act on Them "
date = "2011-09-11"
description = "Hold Core Beliefs -- Act on Them"
tags = [
    "agile",
    "collaboration",
]
+++

 I had a recent e-mail exchange from a departing employee that sparked an interesting follow-up mail thread about whether my organization was making strides in our Agile journey.  Many may have taken the comments as a punch in the gut.   I took it as an opportunity to point out just a few of the things that our team had accomplished.    Here was the essence of that message:

Who would have thought just two years that in a single week we could:

- Execute a release branch in less than 24 hours.   (for comparison other products in my organization, this event takes weeks)
- Put 9 builds into our automated infrastructure that runs 10,000’s of automated tests -- including expanded functional testing through the User Interface.
- Produce 4 builds in row that passed 100% of those tests.
- All four of those test runs required no manual intervention
    Doing all of the above while physically moving the entire team and all our test client machines!

Not a bad list at all.   Perhaps what had the team a bit down was that we had historically been criticized for executing on the list above rather than congratulated for it.  I went on to say to say:

We have done many things that I am hard pressed to believe have been done **anywhere**.    

Since this was perhaps a bold statement, it warranted some follow-up discussion.   At various team meetings, I suggested that if anyone knew where this was being done, I wanted to speak with that organization.    One suggestion that came from the team was the Eclipse Project.   I agreed with the suggestion since many of my core beliefs  that drove action towards these accomplishments above are centered on three key principles I first heard from the folks at Eclipse:

- Always Be Delivering
- Not All Builds are Created Equal
- Consume Your Own Output

I will write a blog post about each of these, starting with the first -- Always Be Delivering.

## Always be delivering.

Each and every build that you produce should be consumable by anyone that wants to consume it.    The primary benefit is to shorten the feedback cycle time.   This holds true for both new feature development and bugs that you may have introduced.   A secondary but crucial benefit is that it can significantly increase the stability of the installation --the act of delivering.  I frequently say that on any project I work on, "Day 1 is the developer proof of concept and Day 2 is the install."     Repeated delivery allows you to spot automation opportunities that can again shorten the feedback cycle time.

Frankly it's shocking how much code is written and committed to source repositories with claims made about it being "done" long before any consumer ever touches the code.   A recent quote from a colleague encapsulated this for me, "We need to be proud of our code before we make it available".    I was a consumer of this group's code.    Of the first four milestone builds that we took from that team, two of them did not even install.     The second installation failure was after providing feedback on the installation experience itself!

 My reply to my colleague was, "We get paid by making our code available to anyone that will give us feedback."   A potential downside of seeking feedback is that you generally get it.   You need a system for managing feedback or at least communicate which builds should be consumed by whom.    This is the subject of the next blog post -- Not All Builds are Created Equal.