+++
author = "Hugo Authors"
title = "Markdown Syntax Guide"
date = "2020-03-11"
description = "Basic Markdown syntax with an ode to 'The Dude'."
tags = [
    "markdown",
    "css",
    "html",
]
+++

I have been using Markdown quite a bit lately. Despite using it regularly, I still find myself searching for some of the that I don't use daily.   This article offers a sample of basic Markdown syntax that I need to remember.

<!--more-->

One of the reasons I took a liking to markdown was that I was a huge fan of Wiki syntax. Once my Wiki I used most frequenly went to only a Rich Text Editor, I missed editing the syntax directly and pushing to page. Markdown then felt just like "going home".


## Headings

The following HTML `<h1>`—`<h6>` elements represent six levels of section headings. `<h1>` is the highest section level while `<h6>` is the lowest.

# H1
## H2
### H3
#### H4
##### H5
###### H6

## Paragraph

Paragraphs couldn't be any simpler than this. 

This is a second paragraph.

## Blockquotes

The blockquote element represents content that is quoted from another source, optionally with a citation which must be within a `footer` or `cite` element, and optionally with in-line changes such as annotations and abbreviations.

#### Blockquote without attribution

> Let me explain something to you. Um, I am not "Mr. Lebowski". You're Mr. Lebowski. I'm the Dude. So that's what you call me. You know, that or, uh, His Dudeness, or uh, Duder, or El Duderino if you're not into the whole brevity thing.
> **Note** that you can use *Markdown syntax* within a blockquote.

#### Blockquote with attribution

> This is a very complicated case Maude. You know, a lotta ins, a lotta outs, lotta what-have-yous. And, uh, a lotta strands to keep in my head, man. Lotta strands in old Duder's head.<br>
> — <cite>The Dude[^1]</cite>

[^1]: The above quote is from the movie [The Big Lebowski](https://www.imdb.com/title/tt0118715/?ref_=ttqt_qt_tt) 1998.

## Code Blocks

#### Code block with backticks

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
```

#### Code block indented with four spaces

    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Example HTML5 Document</title>
    </head>
    <body>
      <p>Test</p>
    </body>
    </html>

#### Code block with Hugo's internal highlight shortcode
{{< highlight html >}}
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
{{< /highlight >}}

## List Types

#### Ordered List

1. First item
2. Second item
3. Third item

#### Unordered List

* List item
* Another item
* And another item

#### Nested list

* Fruit
  * Apple
  * Orange
  * Banana
* Dairy
  * Milk
  * Cheese

## If It's not above

Here are two handy quick references:
* https://www.markdownguide.org/basic-syntax/
* https://www.markdownguide.org/cheat-sheet/

