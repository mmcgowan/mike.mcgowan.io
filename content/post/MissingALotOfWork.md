+++
author = "Mike McGowan"
title = "Noticed, You Have Been Missing A Lot of Work"
date = "2024-01-31"
description = "Retirement - yes, it is an adjustment"
tags = [
    "Retirement",
    "Movies",
]
+++

Fans of the movie "Office Space" will recognize the title. If you have not seen the movie, here is the [relevant clip](https://youtu.be/fiAQIddpO6Y?si=fevI-8o3_laA5Q7w). I mention it since I am a few weeks into retirement. I attended a happy hour for another colleague the first week I was not working. It was suggested that in week 1 I was still vacation mode and  I would be missing work by week 3. I am now in my 4th week - most definitely not bored and I would have to say, "I would exactly say I have been missing it".

## Here's what I am currently enjoying

* No need to "fit in" exercising, eating right, getting enough sleep.
* Having time to connect with old friends.
* A lack of "time pressure". I enjoy lot's of things, being "on deadline" is not one of them.
* More time to read.
* I could go on.

## Here's what I have realized

* For those that say, "it's an adjustment" - of course it is. The same would be true of taking another job.
* Your job is what you structure your entire day around. Without a job, you do need something to structure your
 day.
* Purpose / A reason to get up - yep, you need that. Give that some time.

For anyone reading this - more later.