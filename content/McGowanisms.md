+++
title = "McGowanisms"
description = "A list of McGowanisms"
date = "2024-02-08"
aliases = ["about-mike", "contact"]
author = "Mike McGowan"
+++


The McGowans have plenty of "isms".  Listed below are some that have been collected over several decades. If you see one that is missing, shoot it to me in an [e-mail](mailto:michaelj.mcgowan@gmail.com). Please include all of the section headers listed below:

* Full Phrase
* Who said it
* Where
* Context
* Why it is memorable/funny?

# The McGowanisms

- [Down By the Pancake House](#down-by-the-pancake-house)
- [What's going on here?](#what-is-going-on-here)
- [These boats aren't built for speed](#boats-not-built-for-speed)
- [Watcha Doin' -- Stuffin?](#watcha-doing-stuffing)
- [Yah, You take yer 'terdy-two](#yah-you-take-yer-terdy-two)
- [I'm trying to find my marbles!](#trying-to-find-my-marbles)
- [I got 40 acres of flax. up nord’a Morris](#i-got-40-acres-nord-a-morris)
- [What lake? Lake *Superior*!!?](#what-lake)
- [Come on, Big Lloyd, you can't let dem beat](#come-on-big-lloyd)
- [I seen 'em cut that cake down at Jubas](#i-seen-em-cut-that-cake)
- [Whose Dogs Are These?](#whose-dogs-are-these)
- [Where Was You? I Never Seen You?](#where-was-you)
- [Worthless Twins, Can't even Win a Game](#worthless-twins)
- [Strange morality We have these days](#strange-morality-these-days)
- [In my day trips like this were reserved for the honeymoon](#in-my-day-trips-like-this-were-reserved-for-the-honeymoon)


## Down By the Pancake House

**Full Phrase**: Yah, we seen you down by 'da pancake house.

**Who said it**:  Mrs. Weber

**Where:**  Between our respective houses over by Frankson

**Context:** For some reason, [Sean McGowan](https://www.wikitree.com/wiki/McGowan-772) was discussing with Mrs. Weber his then state of employment, telling her that he worked at the Midway Pancake House and this was her response.

**Why it is memorable/funny:**  It is a classic example of western Minnesota/northern Iowa rural folk speak: i.e. "seen" for "saw," with a touch of old-time German, i.e. "by" for "at,"  and "da" for "the.” 

**Other memorable quotes from Mrs. Weber**
1. "Is 'da paper troo?”
2. "No, my Bobby he likes 'da funnies!”
3. To Dan "You must know 'da presi-dent;my Bobby doesn't get to come home as much as you do."

## What is going on here

**Full Phrase**: What's Going on Here?

**Who said it**: James Govern, Sr.

**Where**: Blue Earth, MN

**Context**: Circulation Day(s) for the Blue Earth Post.

**Why is it memorable/funny**: Stuffing (see "Watcha Doin' Stuffing?") was not a particularly tough task.   It took the speaker a bit to realize he was performing the task incorrectly.   In the decades since, this has been used to describe any simple task performed incorrectly.

## Boats not built for speed

**Full Phrase**: These boats aren't built for speed

**Who said it**: The owner of the Beaver Lake Resort

**Where**: Kimball, MN

**Context**: [Vince](https://www.wikitree.com/wiki/McGowan-3728), Mike, and Geoff White were renting a row boat at the Beaver Lake Resort

**Why is it memorable/funny**: When we asked for a second set of oars for the row boat, the owner asked what we wanted it for. Geoff responded with a smirk, "Oh I don't know, some water skiing."  The owner thinking that Geoff was serious, responded with the infamous line.

## Watcha doing stuffing

**Full Phrase**: Watcha Doin' -- Stuffin?

**Who said it**: James Govern, Sr.

**Where**: Blue Earth, MN

**Context**: Circulation Day(s) for the Blue Earth Post.

**Why is it memorable/funny**:  A classic "statement of the obvious"  Since 90% of the work in the circulation department involved inserting section 2 of the paper into section 1 (aka "stuffin") that was indeed why everyone was in the basement of the newspaper office.


## Yah you take yer terdy two

**Full Phrase**: Yah, You take yer 'terdy-two

**Who said it**: Norbert Eischens   

**Where**: Appleton, MN

**Context**: Norbert liked to count out the newspapers for the paper carriers.    Sean wanted to count his own.

**Why is it memorable/funny**: Norbert delivered this line with a bit of an "edge" to it.   That only compounded Sean's well known affinity for rural America speak.

## Trying to find my marbles

**Full Phrase**: I'm trying to find my marbles!

**Who said it**: Mike

**Where**: Crawling under the daybed at Battle Lake

**Context**:(Marty & Betty McGowan family) replying to Dad's inquiry, "What's going on in there?"

**Why is it funny/memorable?**:  Dad didn't like bothersome noises, particularly while reading the paper. (anyone remember Dad's reaction to a squeaky styrofoam cooler while driving to/from the lake?). Additionally, the jokes about a person "loosing their marbles" just write themselves.

## I got 40 acres nord a Morris

**Full Phrase**: I got 40 acres of flax. up nord’a Morris.  More generally anything that was "north" was is "Up nord'a Morris"

**Who said it**: Dan -- look to you on this one

**Where**:  the road to Clitheral
 
**Context**: to any # of appleton citizens

**Why is it funny/memorable:** “No Kiddin”  reply, as if it were true

## What Lake?

**Full Phrase**: What lake?   Lake *Superior*!!? 

**Who said it**: Martin McGowan, Jr.

**Where**: Blue Earth, MN

**Context**: Before leaving town, Dad gave the use of the Blue Earth Post van to Brendan.    He had been given permission to use it for two things that generously would have been 10 miles.    Dad had find that it had been driven 275 miles.   He confronted Brendan Sunday evening.

**Why is it memorable/funny**: Brendan 1st response was that he had taken the van "to the lake" (meaning Bass Lake some 15 miles from Blue Earth).    Dad's response was calm until he  got to the word "Superior" whence he was fully "worked up".    Blue Earth is a good 4-5 hours and some 290 miles away.

## Come on big lloyd

**Full Phrase**:  Come on, Big Lloyd, you can't let dem beat 

**Who said it**: A Basketball fan for the now long gone St. Paul Mechanics Arts HS

**Where**: St. Paul, MN

**Context**: HS Basketball game between Cretin HS and Mechanic Arts

**Why is it memorable/funny**:  No family members were present, but due to Sean's enjoyment of a grammatical error, thought it would be "fun" to share with his siblings.    Little did he realize that he had just given himself a name that he would take to his grave.   RIP "Big Lloyd"

## I seen em cut that cake

**Full Phrase**: I seen 'em cut that cake down at Jubas 

**Who said it**: James Govern, Sr.

**Where**: Blue Earth, MN

**Context**: Prior to a a church social at St. Peter & Paul Catholic church. The speaker's son was a baker at Juba's.

**Why is it memorable/funny:** In the list of fun things to do in Blue Earth, this would not top too many lists. 

## Whose dogs are these? 

**Phrase**: Whose Dogs Are These? 

**Who said it**: James Govern, Sr.

**Where**: Blue Earth, MN

**Context**: A tour was being given of the offices of the Blue Earth Post. The tour included some dog owners with their leashed dogs.

**Why is it memorable/funny:** A statement of the obvious. 

## Where was you?

**Full Phrase**: Where Was You? I Never Seen You? 

**Who said it**: A farmer from Iowa

**Where**: Rake, Iowa

**Context**: Sean and Dan found a watering hole in Rake, Iowa -- the drinking age was lower than in Minnesota.

**Why is it memorable/funny**: Sean always enjoyed a good grammatical error especially when they were strung together.

## Worthless twins

**Full Phrase**: Worthless Twins, Can't even Win a Game 

**Who said it**:  Kevin McGowan     

**Where**: St. Paul, Minnesota

**Context**: Kev had just heard on the radio that the Minnesota Twins had lost their most recent game.

**Why is it memorable/funny**:  In addition to the anger in Kev's voice, he decided that the radio needed to be thrown across the room.

## Strange morality these days

**Full Phrase**: Strange morality we have these days.

**Who said it**:  Betty McGowan     

**Where**: St. Paul, Minnesota

**Context**: While driving by a home for expectant un-wed mothers, Betty noticed that an addition was being made to the home sometime in the late 1960's or early 70/s

**Why is it memorable/funny**: A timeless comment as history continues to "lurch left".

## In my day trips like this were reserved for the honeymoon

**Full Phrase**: In my day, trips like this were reserved for the honeymoon

**Who said it**:  Betty McGowan     

**Where**: St. Paul, Minnesota

**Context**: Vince asked to borrow the family second car for a trip to the Boundary Waters with 2 gals and another guy, all unmarried. Betty said he couldn’t use it because…”

**Why is it memorable/funny**: Betty's "day" was 30 years prior, Vince was 21, and had recently returned from Vietnam - he no longer felt the need to follow her whims.