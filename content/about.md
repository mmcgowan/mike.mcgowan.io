+++
title = "About"
description = "About Mike McGowan"
date = "2020-11-06"
aliases = ["about-mike", "contact"]
author = "Mike McGowan"
+++

I'm an retired software professional, father, sports nut and all around geek. I enjoy drinking beer, just not making it. When in doubt at a restaurant, I have two defaults if there is pressure to order: #4 (quite often #4 is a Club Sandwitch) and a [Summit EPA](https://www.summitbrewing.com/brews/extra-pale-ale/)

When I retire I hope to I might try:
- Learning to [repair bicycles](https://exploringwild.com/bicycle-repair-maintenance-do-it-yourself/)

- [tie flys](https://www.orvis.com/fly-tying) - This is something I thought I might try, but now I am just happy to buy flys.

- Continue to live under the delusion that the [Minnesota Twins](https://mlb.com/twins) might win the World Series again.
